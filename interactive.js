"use strict";

/**
 * This is an interactive client that can be controlled with the keyboard.
 * You can use the keys w,a,s,d to move you player through the maze.
 * The program can be terminated with Ctrl+x.
 *
 * This is only an example for the usage of the client.
 */
require('dotenv').config();

//The maze client to talk to the server:
const MazeClient = require('./lib/MazeClient');

/**
 * The player name.
 * @type {string}
 */
const playerName = process.env.PLAYER_NAME;

/**
 * The token to authenticate against the server.
 * @type {string}
 */
const token = process.env.TOKEN;

/**
 * The server url.
 * @type {string}
 */
const serverUrl = process.env.SERVER_URL;

const stdin = process.stdin;

// without this, we would only get streams once enter is pressed
stdin.setRawMode( true );

// resume stdin in the parent process (node app won't quit all by itself
// unless an error or process.exit() happens)
stdin.resume();

// i don't want binary, do you?
stdin.setEncoding( 'utf8' );

(async ()=>{
  const client = new MazeClient(serverUrl, playerName, token);

  try {
    console.log('Trying to connect...');
    await client.connect();
    console.log('Connected.');
  } catch (error) {
    console.error(error);
    process.exit(1);
  }

  // on any data into stdin
  stdin.on( 'data', async key => {
    console.log('KEY', key);
    // ctrl-c ( end of text )
    if ( key === '\u0003' ) {
      process.exit();
    }
    try {
      switch (key) {
        case 'w':
          console.log('Move up');
          await client.move('up');
          break;
        case 'd':
          console.log('Move right');
          await client.move('right');
          break;
        case 's':
          console.log('Move down');
          await client.move('down');
          break;
        case 'a':
          console.log('Move left');
          await client.move('left');
          break;
        default:
          break;
      }
    } catch (error) {
      console.error(error)
    }
  });

})();
