# Maze Client

This is the client for the maze server.
## Install

Clone the repo and install the needed modules:

```bash
npm install
```

## Configuration

First you need to create a `.env` file in the root directory of the project to configure all needed settings.
You can take `.env_example` as a template.

You need to set the server url and your player name.
It's ok to leaf the `TOKEN` untouched, as this is not used yet.

```ini
PLAYER_NAME=LukeSkywalker
TOKEN=01
SERVER_URL=http://localhost:3000
```

## Getting started

Now you can start to code you own client for the maze.
For this you can open `index.js` and start implementing your maze client.
In this way, you don't need to deal with the API itself, as everything is already in place and you can focus on the algorithm instead.

To start you client just run `node index.js`.
During development you can also run

```bash
npm run dev
```

to have hot-reload in place and boost you development in this way.


