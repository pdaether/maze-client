var io = require('socket.io-client');

/**
 * Just for a nicer output.
 */
const directionSigns = {
  'up': '⬆️',
  'down': '⬇️',
  'left': '⬅️',
  'right': '➡️'
}
const EventEmitter = require('events');
/**
 * A simple client for the maze server, using the
 * web sockets API.
 *
 * @example
 * const client = new MazeClient(SERVER_URL, LAYER_NAME, TOKEN);
 * try {
 *   client.on('error', () => {...})
 *   await client.connect();
 *   await client.move('up');
 * } catch (error) {
 *   // error handling
 * }
 *
 * @event error Fired if an error from the server occurs.
 */
class MazeClient extends EventEmitter {
  constructor(serverUrl, name, token) {
    super();
    this.serverUrl = `${serverUrl}/clients`;
    this.name = name;
    this.token = token;
  }

  /**
   * Connects to the server and registers the player.
   * This method need to be called always first.
   *
   * @returns {Promise} Resolved on success, rejected otherwise.
   */
  connect() {
    console.log(`🔗 Connecting to ${this.serverUrl}`);
    this.conn = io.connect(this.serverUrl);

    return new Promise((res, rej) => {
      let connected = false;
      this.conn.on('connect', () => {
        this.conn.emit('register', {name: this.name}, data => {
          this.maze = data;
          connected = true;
          res();
        });
      });
      this.conn.on('error', (err) => {
        console.log('⚠️  MazeClient ERROR:', err);
        if(this.listenerCount('error') > 0){
          this.emit('error', err);
        }
        connected ?? rej();
      });
    });
  }

  /**
   * Moves the player in the given direction.
   *
   * @param {up|left|down|right} direction The direction in which the player should move.
   * @returns {Promise} Resolved with the new position when the move was successful, rejected otherwise.
   */
  move(direction) {
    return new Promise((res, rej) => {
      console.log(`${directionSigns[direction]}  Moving ${direction}`);
      this.conn.emit('move', {direction: direction}, (err, pos) => {
        if(err !== null){
          console.log(`⚠️  ${err}`);
          return rej(err);
        }
        res(pos);
      });
    });
  }

  /**
   * Return the maze plan as a 2-dimensional array.
   * The array values could be:
   * - '0' = A wall, the player could not move here.
   * - '1' = A corridor
   * - '2' = The gate of of the maze and the goal for the player.
   *
   * Please note that the y coordinate comes first!
   *
   * You can als use the map MazeClient.FIELD_TYPES for dealing with the types.
   *
   * @example
   * const plan = client.getMazePlan();
   * const start = client.getStartPosition();
   * plan[start.y][start.x];
   *
   * @returns {array} The maze plan as a 2-dimensional array
   */
  getMazePlan() {
    return [...this.maze.plan];
  }

  /**
   * Returns the starting position of the player in the maze.
   *
   * @returns {{x: number, y: number}} The starting position.
   */
  getStartPosition() {
    return this.maze.start;
  }

  /**
   * Return the type of the map filed at the given position:
   * - 0 = Wall
   * - 1 = Corridor
   * - 2 = Goal
   *
   * You can als use the map MazeClient.FIELD_TYPES for dealing with the types.
   *
   * @param {{x:number, y:number}} A position object with x and y coordinates.
   * @returns {0,1,2} The type of the field.
   */
  getPositionType(position){
    return this.maze.plan[position.y][position.x];
  }
}

MazeClient.FIELD_TYPES = {
  WALL : 0,
  CORRIDOR: 1,
  GOAL: 2
};

module.exports = MazeClient;
