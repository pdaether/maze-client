"use strict";

require('dotenv').config();

//The maze client to talk to the server:
const MazeClient = require('./lib/MazeClient');

/**
 * The player name.
 * @type {string}
 */
const playerName = process.env.PLAYER_NAME;

/**
 * The token to authenticate against the server.
 * @type {string}
 */
const token = process.env.TOKEN;

/**
 * The server url.
 * @type {string}
 */
const serverUrl = process.env.SERVER_URL;

(async ()=>{
  const client = new MazeClient(serverUrl, playerName, token);

  try {
    await client.connect();
    console.log(`Connected as ${playerName}.`);
    //Now you can do your stuff like:
    // await client.move('up');
    // Have fun! 🤩 💻
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
